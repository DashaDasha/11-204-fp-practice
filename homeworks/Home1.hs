{- 
 -
 -                4
 - pi = -------------------
 -                1^2
 -       1 + --------------
 -                   3^2
 -            2 + ---------
 -                     5^2
 -                 2 + ----
 -                      ...
 -}
pi1 n | n > 0 = 4 / (1 + (count 1 n))
    where count a 1 = a * a
          count a n = (a * a) / (2 + (count (a + 2) (n-1)))

{-
 -                  1^2
 - pi = 3 + -------------------
 -                   3^2
 -           6 + -------------
 -                     5^2
 -                6 + ------
 -                     ...
 -}
pi2 n | n > 0 = 3 + (1 / (count 3 n))
    where count a 1 = 6
          count a n = (6 + (a * a) / count (a + 2) (n - 1))

{-
 -                 4
 - pi = ------------------------
 -                   1^2
 -       1 + ------------------
 -                    2^2
 -            3 + -----------
 -                     3^2
 -                 5 + ---
 -                     ...
 -}
pi3 n | n > 0 = 4 / (count 1 n 1)
    where count a 1 f = f*f
          count a n f = (a + ((f * f) /  (count (a + 2)(n - 1)(f+1))))
 
{-       4     4     4     4
 - pi = --- - --- + --- - --- + ...
 -       1     3     5     7
 -}
pi4 n | n > 0 = 4 + minus 3 n
    where minus a 1 = 0
          minus a n = - 4 / a + plus (a+2) (n-1)
          plus a 1 = 0
          plus a n = 4 / a + minus (a+2) (n-1)

{-             4         4         4         4
 - pi = 3 + ------- - ------- + ------- - -------- + ...
 -           2*3*4     4*5*6     6*7*8     8*9*10
 -}
pi5 n | n > 0 = 3 + plus 2 n
    where mult a = a*(a+1)*(a+2)
    	  plus a 1 = 0
          plus a n =  4 / mult a  + minus (a+2) (n-1)
          minus a 1 = 0
          minus a n = - 4 / mult a  + plus (a+2) (n-1)

{-
       x^1     x^2
e^x = ----- + ----- + ...
        1!      2!
-}
e x n | n > 0 = count x n 1
    where count x 1 a = 1
          count x n a = x^a / factorial a + count x (n-1) (a+1)
          factorial n = if n==0
              then 1
              else n*factorial (n-1)
