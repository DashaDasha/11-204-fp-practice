
-- | Нахождение максимум и его индекс 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex [] = (-1, 0)
myMaxIndex (x:xs) = find (0, x) 1 xs
    where find maxElem i [] = maxElem
          find maxElem i (x:xs) = if x > (snd maxElem)
                          then find (i, x) (i + 1) xs
                          else find maxElem (i + 1) xs

-- | Количество элементов, равных максимальному
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Integer] -> Int
maxCount [] = 0
maxCount (x:xs) = find (1, x) xs
    where find a [] = (fst a)
          find a (x:xs) = if x > (snd a)
                          then find (1, x) xs
                          else if x == (snd a)
                                then find ((fst a) + 1, x) xs
                                else find a xs

-- | Количество элементов между минимальным и максимальным
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0
countBetween :: [Integer] -> Int
countBetween [] = 0
countBetween (x:xs) = find ((0, x), (0, x)) 1 xs
    where find a i [] = fromIntegral(fst(fst a) - fst(snd a))
          find a i (x:xs) = if x > (snd (fst a))
                             then find ((i, x), (snd a)) (i + 1) xs
                             else if x < (snd (snd a))
                                   then find ((fst a), (i, x)) (i + 1) xs
                                   else find a (i+1) xs
           
