
-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt a arr = cut a arr
    where cut a [] = []
          cut a (x:xs) = if x==a
                                then cut a xs
                                else x :cut a xs
             
-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesInt :: Int -> [Int] -> [Int]
findIndicesInt a arr= find a arr 0
    where find a [] i = []
          find a (x:xs) i = if a==x
						then i: find a xs (i+1)
						else find a xs (i+1)

-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo n = count 1 1 1 (n - 3)
        where count a b c 1 = a + b + c
              count a b c n | n > 0 = count b c (a + b + c) (n - 1)
              count a b c n | n <= 0 = 1

-- Тип Цвет, который может быть Белым, Черным, Красным, Зелёным, Синим, либо Смесь из трёх чисел (0-255)
-- операции сложение :: Цвет -> Цвет -> Цвет
-- (просто складываются интенсивности, если получилось >255, то ставится 255)
-- ПолучитьКрасныйКанал, ПолучитьЗелёныйКанал, ПолучитьСинийКанал :: Цвет -> Int
data Color = Rgb { r :: Int, g :: Int, b :: Int }

white = Rgb 255	255	255
black = Rgb 0	0 	0
red   = Rgb 255 0	0
green = Rgb 0	255	0
blue  = Rgb 0	0	255

addColors :: Color -> Color -> Color
addColors (Rgb r1 g1 b1) (Rgb r2 g2 b2) = Rgb (getCorrectColor(r1 + r2))
                                              (getCorrectColor(g2 + g1))
                                              (getCorrectColor(b1 + b2))
                                              where getCorrectColor :: Int -> Int
                                                    getCorrectColor a | a > 255 = 255
                                                                      | otherwise = a

getRed :: Color -> Int
getRed (Rgb r _ _) = r

getBlue :: Color -> Int
getBlue (Rgb _ g _) = g
 
getGreen :: Color -> Int
getGreen (Rgb _ _ b) = b