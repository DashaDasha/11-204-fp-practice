-- арифметические прогрессии в простых числах длины 3 с расстоянием k
-- например, take 2 (arith 2) = [[5,11,17], [7,13,19]]
arith :: Int -> [[Int]]
arith k = filter isProgres $ progres k
      where isProgres (x: y: z:xs) | x-y == y-z = True
                                   | otherwise = False
            progres k = let primes' = getKTail k primes
                            primes'' = getKTail (2*k) primes
                            in zip3' primes primes' primes''
            getKTail k arr | k == 0 = arr
                           | otherwise = getKTail (k-1) $ tail arr
            zip3' (a:as) (b:bs) (c:cs) = [a, b, c] : zip3' as bs cs
            zip _      _    _  = []
            primes = filter isPrime [1..]
            isPrime 1 = False
            isPrime 2 = True
            isPrime n | even n = False
                      | otherwise = null $ filter (\i -> n`mod`i == 0) $ takeWhile (\i -> i*i <=n) [3,5..]

-- положение ферзей на шахматной доске nxn, при котором они не бьют друг друга
-- список номеров горизонталей, на которых находятся ферзи
-- например, Board [1,2,3,4,5,6,8,7] -- это такое расположение
--  +--------+
-- 8|      ♕ |
-- 7|       ♕|
-- 6|     ♕  |
-- 5|    ♕   |
-- 4|   ♕    |
-- 3|  ♕     |
-- 2| ♕      |
-- 1|♕       |
-- -+--------+
--  |abcdefgh|

newtype Board = Board { unBoard :: [Int] } deriving (Eq,Show)

queens :: Int -> [Board]
queens n = undefined

-- Белые начинают и дают мат в два хода
-- 
-- Белые: Фf8 g4 Крh2
-- Черные: g5 h5 a4 Крh4
-- 
-- (написать перебор всех вариантов полуходов длины три,
-- вернуть список последовательностей полуходов, ведущих
-- к решению; до этого определить необходимые типы)
-- См. https://en.wikipedia.org/wiki/Chess_notation

-- Определите так, как нужно
type Move = Int -- ???

solutions :: [[Move]]
solutions = undefined

